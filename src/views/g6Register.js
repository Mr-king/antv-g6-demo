import G6 from "@antv/g6";
function Int_registerNode() {
  G6.registerNode(
    "zdy_diamond",
    {
      draw(cfg, group) {
        // 如果 cfg 中定义了 style 需要同这里的属性进行融合
        const keyShape = group.addShape("path", {
          attrs: {
            path: this.getPath(cfg), // 根据配置获取路径
            stroke: "#000", // 颜色应用到描边上，如果应用到填充，则使用 fill: cfg.color
          },
          // must be assigned in G6 3.3 and later versions. it can be any value you want
          name: "path-shape",
          // 设置 draggable 以允许响应鼠标的图拽事件
          draggable: true,
        });
        if (cfg.label) {
          // 如果有文本
          // 如果需要复杂的文本配置项，可以通过 labeCfg 传入
          // const style = (cfg.labelCfg && cfg.labelCfg.style) || {};
          // style.text = cfg.label;
          const label = group.addShape("text", {
            // attrs: style
            attrs: {
              x: 0, // 居中
              y: 0,
              textAlign: "center",
              textBaseline: "middle",
              text: cfg.label,
              fill: "#666",
            },
            // must be assigned in G6 3.3 and later versions. it can be any value you want
            name: "text-shape",
            // 设置 draggable 以允许响应鼠标的图拽事件
            draggable: true,
          });
        }
        return keyShape;
      },
      // 返回菱形的路径
      getPath(cfg) {
        const size = cfg.size || [40, 40]; // 如果没有 size 时的默认大小
        const width = size[0];
        const height = size[1];
        //  / 1 \
        // 4     2
        //  \ 3 /
        const path = [
          ["M", 0, 0 - height / 2], // 上部顶点
          ["L", width / 2, 0], // 右侧顶点
          ["L", 0, height / 2], // 下部顶点
          ["L", -width / 2, 0], // 左侧顶点
          ["Z"], // 封闭
        ];
        return path;
      },
    },
    "single-node"
  );
  G6.registerEdge(
    "circle-running",
    {
      afterDraw(cfg, group) {
        // 获取组中的第一个形状，它是这里的边缘路径
        const shape = group.get("children")[0];
        // 边缘路径的起始位置
        const startPoint = shape.getPoint(0);
        // 添加蓝色圆形
        const circle = group.addShape("circle", {
          attrs: {
            x: startPoint.x,
            y: startPoint.y,
            fill: "#1890ff",
            r: 3,
          },
          name: "circle-shape",
        });
        // 动画为蓝圈
        circle.animate(
          (ratio) => {
            // 每一帧的操作。比率范围从0到1表示动画的进度。返回修改后的配置。
            // 根据比例获取边缘上的位置
            const tmpPoint = shape.getPoint(ratio);
            // 这里返回修改后的配置，这里是x和y
            return {
              x: tmpPoint.x,
              y: tmpPoint.y,
            };
          },
          {
            repeat: true, // 是否重复执行动画
            duration: 3000, // 执行一次的持续时间
          }
        );
      },
    },
    "polyline" // e扩展内置边缘'cubic'
  );
  G6.registerNode('card-node', {
    draw: function drawShape(cfg, group) {
      const r = 2;
      const color = '#5B8FF9';
      const w = cfg.size[0];
      const h = cfg.size[1];
      const shape = group.addShape('rect', {
        attrs: {
          x: -w / 2,
          y: -h / 2,
          width: w, //200,
          height: h, // 60
          stroke: color,
          radius: r,
          fill: '#fff',
        },
        name: 'main-box',
        draggable: true,
      });

      group.addShape('rect', {
        attrs: {
          x: -w / 2,
          y: -h / 2,
          width: w, //200,
          height: h / 2, // 60
          fill: color,
          radius: [r, r, 0, 0],
        },
        name: 'title-box',
        draggable: true,
      });

      // title text
      group.addShape('text', {
        attrs: {
          textBaseline: 'top',
          x: -w / 2 + 16,
          y: -h / 2 + 16,
          lineHeight: 80,
          text: cfg.label,
          fill: '#fff',
        },
        name: 'title',
        draggable: true,
      });
      group.addShape('marker', {
        attrs: {
          x: w / 2,
          y: 0,
          r: 6,
          cursor: 'pointer',
          symbol: G6.Marker.collapse,
          stroke: '#666',
          lineWidth: 1,
          fill: '#fff',
        },
        name: 'collapse-icon',
      });
      group.addShape('text', {
        attrs: {
          textBaseline: 'top',
          x: -w / 2 + 16,
          y: -h / 2 + 48,
          lineHeight: 40,
          text: cfg.content,
          fill: 'rgba(0,0,0, 1)',
        },
        name: `description`,
      });
      return shape;
    },
    // setState(name, value, item) {
    //   if (name === 'collapsed') {
    //     const marker = item.get('group').find((ele) => ele.get('name') === 'collapse-icon');
    //     const icon = value ? G6.Marker.expand : G6.Marker.collapse;
    //     marker.attr('symbol', icon);
    //   }
    // },
  });
  G6.registerNode(
    'circle-animate',
    {
      afterDraw(cfg, group) {
        const shape = group.get('children')[0];
        shape.animate(
          (ratio) => {
            const diff = ratio <= 0.5 ? ratio * 10 : (1 - ratio) * 10;
            return {
              r: cfg.size / 2 + diff,
            };
          },
          {
            repeat: true,
            duration: 3000,
            easing: 'easeCubic',
          },
        );
      },
    },
    'circle',
  );
}
export {
  Int_registerNode
}