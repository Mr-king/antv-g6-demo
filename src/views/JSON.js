let gonphData={
	"nodes": [
		{
			"id": "node1",
			"x": 81,
			"y": 0,
			"size": 60,
			"type": "circle",
			"label": "circle圆形",
			"comboId": "combo",
			"style": {
				"active": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 2,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10
				},
				"selected": {
					"fill": "rgb(255, 255, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 4,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"highlight": {
					"fill": "rgb(223, 234, 255)",
					"stroke": "#4572d9",
					"lineWidth": 2,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"inactive": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(191, 213, 255)",
					"lineWidth": 1
				},
				"disable": {
					"fill": "rgb(250, 250, 250)",
					"stroke": "rgb(224, 224, 224)",
					"lineWidth": 1
				}
			},
			"depth": 18
		},
		{
			"id": "node2",
			"x": 286,
			"y": -100,
			"size": [
				90,
				50
			],
			"type": "rect",
			"label": "rect矩形",
			"comboId": "combo",
			"style": {
				"active": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 2,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10
				},
				"selected": {
					"fill": "rgb(255, 255, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 4,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"highlight": {
					"fill": "rgb(223, 234, 255)",
					"stroke": "#4572d9",
					"lineWidth": 2,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"inactive": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(191, 213, 255)",
					"lineWidth": 1
				},
				"disable": {
					"fill": "rgb(250, 250, 250)",
					"stroke": "rgb(224, 224, 224)",
					"lineWidth": 1
				}
			},
			"depth": 17
		},
		{
			"id": "node3",
			"x": 470,
			"y": -94,
			"size": [
				80,
				40
			],
			"type": "ellipse",
			"label": "ellipse椭圆",
			"labelCfg": {
				"position": "bottom",
				"offset": 5
			},
			"style": {
				"active": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 2,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10
				},
				"selected": {
					"fill": "rgb(255, 255, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 4,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"highlight": {
					"fill": "rgb(223, 234, 255)",
					"stroke": "#4572d9",
					"lineWidth": 2,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"inactive": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(191, 213, 255)",
					"lineWidth": 1
				},
				"disable": {
					"fill": "rgb(250, 250, 250)",
					"stroke": "rgb(224, 224, 224)",
					"lineWidth": 1
				},
				"fill": "#fa8c16",
				"stroke": "#000",
				"lineWidth": 2
			},
			"comboId": "combo",
			"depth": 16
		},
		{
			"id": "node4",
			"x": 680,
			"y": -101,
			"size": [
				100,
				80
			],
			"type": "diamond",
			"label": "diamond菱形",
			"comboId": "combo",
			"style": {
				"active": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 2,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10
				},
				"selected": {
					"fill": "rgb(255, 255, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 4,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"highlight": {
					"fill": "rgb(223, 234, 255)",
					"stroke": "#4572d9",
					"lineWidth": 2,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"inactive": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(191, 213, 255)",
					"lineWidth": 1
				},
				"disable": {
					"fill": "rgb(250, 250, 250)",
					"stroke": "rgb(224, 224, 224)",
					"lineWidth": 1
				}
			},
			"depth": 15
		},
		{
			"id": "node4111",
			"x": 580,
			"y": -301,
			"size": [
				100,
				80
			],
			"type": "zdy_diamond",
			"label": "自定义菱形",
			"draggable": true,
			"style": {}
		},
		{
			"id": "node5",
			"x": 640,
			"y": 100,
			"type": "triangle",
			"label": "triangle三角形",
			"labelCfg": {
				"position": "right",
				"offset": 5
			},
			"comboId": "combo",
			"style": {
				"active": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 2,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10
				},
				"selected": {
					"fill": "rgb(255, 255, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 4,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"highlight": {
					"fill": "rgb(223, 234, 255)",
					"stroke": "#4572d9",
					"lineWidth": 2,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"inactive": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(191, 213, 255)",
					"lineWidth": 1
				},
				"disable": {
					"fill": "rgb(250, 250, 250)",
					"stroke": "rgb(224, 224, 224)",
					"lineWidth": 1
				}
			},
			"depth": 14
		},
		{
			"id": "node6",
			"x": 117,
			"y": 188,
			"size": 65,
			"type": "star",
			"label": "star五角星",
			"comboId": "combo",
			"style": {
				"active": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 2,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10
				},
				"selected": {
					"fill": "rgb(255, 255, 255)",
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 4,
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"highlight": {
					"fill": "rgb(223, 234, 255)",
					"stroke": "#4572d9",
					"lineWidth": 2,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"inactive": {
					"fill": "rgb(247, 250, 255)",
					"stroke": "rgb(191, 213, 255)",
					"lineWidth": 1
				},
				"disable": {
					"fill": "rgb(250, 250, 250)",
					"stroke": "rgb(224, 224, 224)",
					"lineWidth": 1
				}
			},
			"depth": 13
		},
		{
			"id": "node7",
			"x": 259,
			"y": 23,
			"size": [
				65,
				20
			],
			"type": "image",
			"img": "/img/tp.73b8afc4.svg",
			"label": "image自定义图片",
			"comboId": "combo",
			"style": {},
			"depth": 12
		},
		{
			"id": "node8",
			"x": 383,
			"y": 250,
			"description": "描述文本xxxxxxxxxxx",
			"type": "modelRect",
			"label": "modelRect文本描述",
			"comboId": "combo",
			"style": {},
			"depth": 11
		}
	],
	"edges": [
		{
			"source": "node1",
			"target": "node2",
			"label": "第1条",
			"type": "circle-running",
			"labelCfg": {
				"autoRotate": true,
				"style": {
					"stroke": "white",
					"lineWidth": 5
				}
			},
			"style": {
				"lineWidth": 2,
				"endArrow": true,
				"stroke": "#873bf4"
			},
			"id": "edge-0.46274038335351911668417561273",
			"startPoint": {
				"x": 108.41243759213894,
				"y": -13.37192077665314,
				"id": "11|||-1"
			},
			"endPoint": {
				"x": 240.5,
				"y": -100,
				"anchorIndex": 0,
				"id": "24|||-10"
			}
		},
		{
			"source": "node2",
			"target": "node3",
			"label": "第2条",
			"type": "polyline",
			"labelCfg": {
				"autoRotate": true,
				"style": {
					"stroke": "white",
					"lineWidth": 5
				}
			},
			"style": {
				"lineWidth": 2,
				"endArrow": true,
				"stroke": "#873bf4"
			},
			"id": "edge-0.96472235311105231668417561287",
			"startPoint": {
				"x": 331.5,
				"y": -100,
				"anchorIndex": 1,
				"id": "33|||-10"
			},
			"endPoint": {
				"x": 429.08283847218615,
				"y": -100,
				"id": "43|||-10"
			}
		},
		{
			"source": "node3",
			"target": "node4",
			"label": "第3条",
			"type": "polyline",
			"labelCfg": {
				"autoRotate": true,
				"style": {
					"stroke": "white",
					"lineWidth": 5
				}
			},
			"style": {
				"lineWidth": 2,
				"endArrow": true,
				"stroke": "#873bf4"
			},
			"id": "edge-0.125758126726873031668417561291",
			"startPoint": {
				"x": 510.91345068937403,
				"y": -95.3637816896458,
				"id": "51|||-10"
			},
			"endPoint": {
				"x": 629.1996094703209,
				"y": -95.3637816896458,
				"id": "63|||-10"
			}
		},
		{
			"source": "node4",
			"target": "node5",
			"label": "第4条",
			"type": "polyline",
			"labelCfg": {
				"autoRotate": true,
				"style": {
					"stroke": "white",
					"lineWidth": 5
				}
			},
			"style": {
				"lineWidth": 2,
				"endArrow": true,
				"stroke": "#873bf4"
			},
			"id": "edge-0.57491508400640551668417561294",
			"startPoint": {
				"x": 671.912375637066,
				"y": -60.35968757625672,
				"id": "67|||-6"
			},
			"endPoint": {
				"x": 647.1022497626309,
				"y": 64.24094985987256,
				"id": "65|||6"
			}
		},
		{
			"source": "node1",
			"target": "node6",
			"label": "第5条",
			"type": "polyline",
			"labelCfg": {
				"autoRotate": true,
				"style": {
					"stroke": "white",
					"lineWidth": 5
				}
			},
			"style": {
				"lineWidth": 2,
				"endArrow": true,
				"stroke": "#873bf4"
			},
			"id": "edge-0.75432723110090551668417561305",
			"startPoint": {
				"x": 86.73620425902966,
				"y": 29.95573335271046,
				"id": "9|||3"
			},
			"endPoint": {
				"x": 105.03173756851521,
				"y": 121.34257204621974,
				"id": "11|||12"
			}
		},
		{
			"source": "node6",
			"target": "node7",
			"label": "第6条",
			"type": "polyline",
			"labelCfg": {
				"autoRotate": true,
				"style": {
					"stroke": "white",
					"lineWidth": 5
				}
			},
			"style": {
				"lineWidth": 2,
				"endArrow": true,
				"stroke": "#873bf4"
			},
			"id": "edge-0.72256957497356611668417561312",
			"startPoint": {
				"x": 171.05991423169752,
				"y": 121.34257204621974,
				"id": "17|||12"
			},
			"endPoint": {
				"x": 250.39393939393938,
				"y": 33,
				"id": "25|||3"
			}
		},
		{
			"source": "node7",
			"target": "node8",
			"label": "第7条",
			"type": "polyline",
			"labelCfg": {
				"autoRotate": true,
				"style": {
					"stroke": "white",
					"lineWidth": 5
				}
			},
			"style": {
				"lineWidth": 2,
				"endArrow": true,
				"stroke": "#873bf4"
			},
			"id": "edge-0.45011163263461621668417561318",
			"startPoint": {
				"x": 264.4625550660793,
				"y": 33,
				"id": "26|||3"
			},
			"endPoint": {
				"x": 290,
				"y": 250,
				"anchorIndex": 0,
				"id": "29|||25"
			}
		},
		{
			"source": "node8",
			"target": "node5",
			"label": "第8条",
			"type": "polyline",
			"labelCfg": {
				"autoRotate": true,
				"style": {
					"stroke": "white",
					"lineWidth": 5
				}
			},
			"style": {
				"lineWidth": 2,
				"endArrow": true,
				"stroke": "#873bf4"
			},
			"id": "edge-0.71151906154168461668417561321",
			"startPoint": {
				"x": 476,
				"y": 250,
				"anchorIndex": 1,
				"id": "48|||25"
			},
			"endPoint": {
				"x": 604.5499668542475,
				"y": 120.61705192852762,
				"id": "60|||12"
			}
		}
	],
	"combos": [
		{
			"id": "combo",
			"label": "默认combo",
			"children": [
				{
					"id": "node1",
					"comboId": "combo",
					"itemType": "node",
					"depth": 12
				},
				{
					"id": "node2",
					"comboId": "combo",
					"itemType": "node",
					"depth": 12
				},
				{
					"id": "node3",
					"comboId": "combo",
					"itemType": "node",
					"depth": 12
				},
				{
					"id": "node4",
					"comboId": "combo",
					"itemType": "node",
					"depth": 12
				},
				{
					"id": "node5",
					"comboId": "combo",
					"itemType": "node",
					"depth": 12
				},
				{
					"id": "node6",
					"comboId": "combo",
					"itemType": "node",
					"depth": 12
				},
				{
					"id": "node7",
					"comboId": "combo",
					"itemType": "node",
					"depth": 12
				},
				{
					"id": "node8",
					"comboId": "combo",
					"itemType": "node",
					"depth": 12
				}
			],
			"depth": 10,
			"type": "rect",
			"size": [
				100,
				100
			],
			"style": {
				"active": {
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 1,
					"fill": "rgb(247, 250, 255)"
				},
				"selected": {
					"stroke": "rgb(95, 149, 255)",
					"lineWidth": 2,
					"fill": "rgb(253, 253, 253)",
					"shadowColor": "rgb(95, 149, 255)",
					"shadowBlur": 10,
					"text-shape": {
						"fontWeight": 500
					}
				},
				"highlight": {
					"stroke": "#4572d9",
					"lineWidth": 2,
					"fill": "rgb(253, 253, 253)",
					"text-shape": {
						"fontWeight": 500
					}
				},
				"inactive": {
					"stroke": "rgb(224, 224, 224)",
					"fill": "rgb(253, 253, 253)",
					"lineWidth": 1
				},
				"disable": {
					"stroke": "rgb(234, 234, 234)",
					"fill": "rgb(250, 250, 250)",
					"lineWidth": 1
				},
				"lineWidth": 1,
				"fillOpacity": 0.5,
				"r": 401.63959835040134,
				"width": 680.3003905296791,
				"height": 427.1403124237433
			},
			"labelCfg": {
				"style": {
					"fontSize": 18,
					"color": "#000"
				}
			},
			"x": 390.65019526483957,
			"y": 71.92984378812835
		}
	]
}