import { type } from "jquery";

const facility = require("../assets/images/tp.svg");
const default_data = {
    nodes: [
        {
            id: "node1",
            x: 81, //节点x轴位置
            y: 0, //节点y轴位置
            size: 60, //图形尺寸
            type: "circle-animate", //节点的形状
            label: "circle圆形", //节点内的文本名称
            comboId: "combo",
            zdyData: "自定义数据",
        },
        {
            id: "node2",
            x: 286,
            y: -100,
            size: [90, 50], //节点的长宽值
            type: "rect",
            label: "rect矩形",
            comboId: "combo",
        },
        {
            id: "node3",
            x: 470,
            y: -94,
            size: [80, 40],
            type: "ellipse",
            label: "ellipse椭圆",
            labelCfg: {
                position: "bottom",
                offset: 5,
            },
            style: {
                fill: "#fa8c16",
                stroke: "#000",
                lineWidth: 2,
            },
            comboId: "combo",
        },
        {
            id: "node4",
            x: 680,
            y: -101,
            size: [100, 80],
            type: "diamond",
            label: "diamond菱形",
            comboId: "combo",
        },
        {
            id: "node4111",
            x: 580,
            y: -301,
            size: [100, 80],
            type: "zdy_diamond",
            label: "自定义菱形",
            draggable: true,
        },
        {
            id: "node5",
            x: 640,
            y: 100,
            type: "triangle",
            label: "triangle三角形",
            labelCfg: {
                position: "right",
                offset: 5,
            },
            comboId: "combo",
        },
        {
            id: "node6",
            x: 117,
            y: 188,
            size: 65,
            type: "star",
            label: "star五角星",
            comboId: "combo",
        },
        {
            id: "node7",
            x: 259,
            y: 23,
            size: [65, 20],
            type: "image",
            img: facility,
            label: "image自定义图片",
            comboId: "combo",
        },
        {
            id: "node8",
            x: 383,
            y: 250,
            description: "描述文本xxxxxxxxxxx",
            type: "modelRect",
            label: "modelRect文本描述",
            comboId: "combo",
        },
        {
            id: "node9",
            label: 'Donut',
            type: 'donut',
            x: 853,
            y: -100,
            size: 60,
            labelCfg: {
                position: 'bottom'
            },
            icon: {
                show: true,
            },
            donutAttrs: {
                prop1: 10,
                prop2: 20,
                prop3: 25
            },
            donutColorMap: {
                prop1: '#8eaade',
                prop2: '#5c7cb8',
                prop3: '#1e3f7d'
            },
            comboId: "combo",
        },
        {
            id: '12453546456',
            type: 'card-node',
            x: 405,
            y: -278,
            label: '标题',
            content: '内容',
            size: [200, 80],
        }
    ],
    edges: [
        {
            source: "node1",
            target: "node2",
            label: "第1条",
            type: "circle-running",
        },
        {
            source: "node2",
            target: "node3",
            label: "第2条",
        },
        {
            source: "node3",
            target: "node4",
            label: "第3条",
        },
        {
            source: "node4",
            target: "node5",
            label: "第4条",
        },
        {
            source: "node1",
            target: "node6",
            label: "第5条",
        },
        {
            source: "node6",
            target: "node7",
            label: "第6条",
        },
        {
            source: "node7",
            target: "node8",
            label: "第7条",
        },
        {
            source: "node8",
            target: "node5",
            label: "第8条",
        },
        {
            source: "node4",
            target: "node9",
            label: "第9条",
        },
    ],
    combos: [
        {
            id: "combo",
            label: "默认combo",
        },
    ],
};
const nodeMap = new Map([
    [
        'combo_rect', {
            label: '新增rect_combo',
            type: "rect"
        }
    ],
    [
        'combo_circle', {
            label: '新增circle_combo',
            type: "circle"
        }
    ],
    [
        'circle', {
            size: 60, //图形尺寸
            type: "circle", //节点的形状
            label: "circle圆形", //节点内的文本名称
        }
    ],
    [
        'donut', {
            label: 'Donut',
            type: 'donut',
            size: 60,
            labelCfg: {
                position: 'bottom'
            },
            icon: {
                show: true,
            },
            donutAttrs: {
                prop1: 10,
                prop2: 20,
                prop3: 25
            },
            donutColorMap: {
                prop1: '#8eaade',
                prop2: '#5c7cb8',
                prop3: '#1e3f7d'
            },
        },
    ],
    [
        'rect', {
            size: [90, 50], //节点的长宽值
            type: "rect",
            label: "rect矩形",
        },
    ],
    [
        'ellipse', {
            size: [80, 40],
            type: "ellipse",
            label: "ellipse椭圆",
            labelCfg: {
                position: "bottom",
                offset: 5,
            },
            style: {
                fill: "#fa8c16",
                stroke: "#000",
                lineWidth: 2,
            },
        },
    ],
    [
        'diamond', {
            size: [100, 80],
            type: "diamond",
            label: "diamond菱形",
        },
    ],
    [
        'triangle', {
            type: "triangle",
            label: "triangle三角形",
            labelCfg: {
                position: "right",
                offset: 5,
            },
        },
    ],
    [
        'star', {
            size: 65,
            type: "star",
            label: "star五角星",
        },
    ],
    [
        'image', {
            size: [65, 20],
            type: "image",
            img: facility,
            label: "image自定义图片",
        },
    ],
    [
        'modelRect', {
            description: "描述文本xxxxxxxxxxx",
            type: "modelRect",
            label: "modelRect文本描述",
        },
    ],
])
const node_edge_list = [
    {
        img: require("../assets/images/yuan.jpg"),
        val: "circle",
    },
    {
        img: require("../assets/images/jx.jpg"),
        val: "rect",
    },
    {
        img: require("../assets/images/sjx.jpg"),
        val: "triangle",
    },
    {
        img: require("../assets/images/ty.jpg"),
        val: "ellipse",
    },
    {
        img: require("../assets/images/lx.jpg"),
        val: "diamond",
    },
    {
        img: require("../assets/images/wjx.jpg"),
        val: "star",
    },
    {
        img: require("../assets/images/mtjx.jpg"),
        val: "modelRect",
    },
    {
        img: require("../assets/images/ttq.jpg"),
        val: "donut",
    },
    {
        img: require("../assets/images/tp.jpg"),
        val: "image",
    },
]
const combo_list = [
    {
        img: require("../assets/images/combo_c.jpg"),
        val: "combo_circle",
    },
    {
        img: require("../assets/images/combo_r.jpg"),
        val: "combo_rect",
    },
]
/**
* 生成一个用不重复的ID
* @param { Number } randomLength 
*/
function getUuiD(randomLength=6) {
    return Number(Math.random().toString().substr(2, randomLength) + Date.now()).toString(36)
}
export {
    nodeMap,
    combo_list,
    node_edge_list,
    default_data,
    getUuiD
}